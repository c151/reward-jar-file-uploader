package com.c15.rewardjarfileupload;

import com.c15.rewardjarfileupload.storage.StorageProperties;
import com.c15.rewardjarfileupload.storage.StorageService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import javax.servlet.ServletContext;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class RewardjarfileuploadApplication {

	public static void main(String[] args) {
		SpringApplication.run(RewardjarfileuploadApplication.class, args);
	}

	@Bean
	CommandLineRunner init(StorageService storageService) {
		return (args) -> {
			storageService.init();
		};
	}
}
